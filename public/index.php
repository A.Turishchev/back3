<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    echo 'Спасибо, результаты сохранены.';
  }
  include('index.html');
  exit();
}

$name = $_POST['field-name'];
$email = $_POST['field-email'];
$date = $_POST['field-date'];
$radio1 = $_POST['radio1'];
$radio2 = $_POST['radio2'];
$field4 = $_POST['field-name-4'];
$name2 = $_POST['field-name-2'];
$check = $_POST['check1'];


$errors = FALSE;
if (empty($_POST['field-name'])) {
  echo "<script type='text/javascript'>alert('Заполните имя.');</script>";
  $errors = TRUE;
}


if ($email == '') {
  echo "<script type='text/javascript'>alert('Заполните e-mail.');</script>";
  $errors = TRUE;
}


if (empty($_POST['field-date'])) {
  echo "<script type='text/javascript'>alert('Заполните дату рождения.');</script>";
  $errors = TRUE;
}


if (empty($_POST['radio1'])) {
  echo "<script type='text/javascript'>alert('Заполните данные.');</script>";
  $errors = TRUE;
}


if (empty($_POST['radio2'])) {
  echo "<script type='text/javascript'>alert('Заполните данные.');</script>";
  $errors = TRUE;
}


if (empty($_POST['field-name-2'])) {
  echo "<script type='text/javascript'>alert('Заполните биографию.');</script>";
  $errors = TRUE;
}


if (empty($_POST['check-1'])) {
  echo "<script type='text/javascript'>alert('Приминте условия.');</script>";
  $errors = TRUE;
}

if ($errors) {
  exit();
}

$user = 'root';
$pass = 'root';
$db = new PDO('mysql:host=localhost;dbname=test', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $stmt = $db->prepare("INSERT INTO form (name,email,date,radio1,radio2,fieldID,name2,check1) VALUE (:name,:email,:date,:radio1,:radio2,:field4,:name2,:check1)");
  $stmt -> execute(['name'=>$name,'email'=>$email,'date'=>$date,'radio1'=>$radio1,'radio2'=>$radio2,'field4'=>$field4,'name2'=>$name2,'check1'=>$check]);
  echo "<script type='text/javascript'>alert('Спасибо, результаты сохранены.');</script>";
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
